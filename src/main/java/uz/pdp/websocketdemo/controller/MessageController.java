package uz.pdp.websocketdemo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.websocketdemo.model.ChatRoom;
import uz.pdp.websocketdemo.model.Message;
import uz.pdp.websocketdemo.model.MessageStatus;
import uz.pdp.websocketdemo.model.User;
import uz.pdp.websocketdemo.payload.MessageDto;
import uz.pdp.websocketdemo.repository.ChatRoomRepository;
import uz.pdp.websocketdemo.repository.MessageRepository;
import uz.pdp.websocketdemo.repository.UserRepository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

//JAMSHID MIRKHADJAYEV


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/messages")
public class MessageController {
    private final MessageRepository messageRepository;
    private final ChatRoomRepository chatRoomRepository;
    private final UserRepository userRepository;


    @GetMapping("/{receiverId}")
    public ResponseEntity<?> getAllMessages(@PathVariable Integer receiverId, @AuthenticationPrincipal User currentUser) {
//        List<Message> all = messageRepository.getAllByReceiverId(receiverId);

// chatId orqali (senderId || receiverId orqali smslarni olish)

        List<Message> all = messageRepository.findAllByChatId(getChatId(receiverId, currentUser.getId()));

        all.forEach(message -> {
            if (Objects.equals(message.getReceiver().getId(), currentUser.getId())) {
                message.setStatus(MessageStatus.READ);
                messageRepository.save(message);
            }
        });

        List<MessageDto> messageDtoList = all.stream().map(message -> new MessageDto(
                message.getId(),
                message.getText(),
                message.getReceiver().getId(),
                message.getSender().getId(),
                message.getSender().getFullName(),
                message.getReceiver().getFullName(),
                message.getStatus().toString(),
                message.getLocalDateTime()
        )).collect(Collectors.toList());

        return ResponseEntity.ok(messageDtoList);
    }

    private Integer getChatId(Integer receiverId, Integer senderId) {
        System.out.println(receiverId);
        System.out.println(senderId);
        ChatRoom bySenderIdAndReceiverId = chatRoomRepository.findBySenderIdAndReceiverId(senderId, receiverId);
        return bySenderIdAndReceiverId.getChatId();
    }
}
