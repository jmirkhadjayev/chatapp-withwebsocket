package uz.pdp.websocketdemo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import uz.pdp.websocketdemo.model.Role;
import uz.pdp.websocketdemo.model.User;
import uz.pdp.websocketdemo.payload.authPayloads.RegisterDto;
import uz.pdp.websocketdemo.repository.RoleRepository;
import uz.pdp.websocketdemo.repository.UserRepository;

import java.util.Arrays;
import java.util.HashSet;

//JAMSHID MIRKHADJAYEV

@Controller
@RequiredArgsConstructor
@RequestMapping("/registration")
public class UserRegisterController {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @ModelAttribute("user")
    public RegisterDto registerDto() {
        return new RegisterDto();
    }

    @GetMapping
    public String showRegistrationForm() {
        return "registration";
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") RegisterDto registerDto) {
        String email = registerDto.getEmail();
        String fullName = registerDto.getFullName();
        String password = registerDto.getPassword();

        Role user_role = roleRepository.save(new Role("USER_ROLE"));
        HashSet<Role> roles = new HashSet<>(Arrays.asList(user_role));

        User user = new User(fullName, email, passwordEncoder.encode(password), false, roles);
        userRepository.save(user);
        return "redirect:/login?success";
    }

}
