package uz.pdp.websocketdemo.controller;

import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import uz.pdp.websocketdemo.model.ChatRoom;
import uz.pdp.websocketdemo.model.Message;
import uz.pdp.websocketdemo.model.User;
import uz.pdp.websocketdemo.payload.MessageDto;
import uz.pdp.websocketdemo.repository.ChatRoomRepository;
import uz.pdp.websocketdemo.repository.MessageRepository;
import uz.pdp.websocketdemo.repository.UserRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

//JAMSHID MIRKHADJAYEV

@Controller
public class ChatController {
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    SimpMessagingTemplate messagingTemplate;
    @Autowired
    ChatRoomRepository chatRoomRepository;


    @MessageMapping("/hello")
//    @SendTo("/topic/chat")
    public void sendMessage(MessageDto messageDto) throws Exception {

        Optional<User> receiverUser = userRepository.findById(messageDto.getReceiverId());
        Optional<User> senderUser = userRepository.findById(messageDto.getSenderId());

        if (senderUser.isEmpty() || receiverUser.isEmpty()) throw new RuntimeException();

        Message savedMessage = messageRepository.save(
                new Message(
                        messageDto.getText(),
                        receiverUser.get(),
                        senderUser.get(),
                        getChatId(messageDto.getReceiverId(), messageDto.getSenderId())
                )
        );

        messageDto.setId(savedMessage.getId());
//        messageDto.setDate(savedMessage.getCreatedAt());
        messageDto.setStatus(savedMessage.getStatus().toString());
        messageDto.setLocalDateTime(savedMessage.getLocalDateTime());

        // user/2/queue/messages
        messagingTemplate.convertAndSendToUser(
                messageDto.getReceiverId().toString(),
                "/queue/messages",
                messageDto
        );
        if (messageDto.getSenderId() != messageDto.getReceiverId()) {
            messagingTemplate.convertAndSendToUser(
                    messageDto.getSenderId().toString(),
                    "/queue/messages",
                    messageDto
            );
        }
    }

    private Integer getChatId(Integer receiverId, Integer senderId) {
        if (chatRoomRepository.existsBySenderIdAndReceiverId(senderId, receiverId)) {
            ChatRoom bySenderIdAndReceiverId = chatRoomRepository.findBySenderIdAndReceiverId(senderId, receiverId);
            return bySenderIdAndReceiverId.getChatId();
        } else {
            Optional<User> sender = userRepository.findById(senderId);
            Optional<User> receiver = userRepository.findById(receiverId);

            if (!sender.isPresent() || !receiver.isPresent()) {
                throw new RuntimeException();
            }

            String chatId = String.format("%s%s", senderId, receiverId);

            if (senderId==receiverId){
                chatRoomRepository.save(new ChatRoom(Integer.valueOf(chatId), receiver.get(), sender.get()));
                return Integer.valueOf(chatId);
            }



            chatRoomRepository.save(new ChatRoom(Integer.valueOf(chatId), sender.get(), receiver.get()));

            //   if (sender.get().getId() != receiver.get().getId())
            chatRoomRepository.save(new ChatRoom(Integer.valueOf(chatId), receiver.get(), sender.get()));
            return Integer.valueOf(chatId);
        }
    }


}
