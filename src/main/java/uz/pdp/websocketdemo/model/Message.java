package uz.pdp.websocketdemo.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

//JAMSHID MIRKHADJAYEV

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity
public final class Message {
    @Id
    @GeneratedValue()
    private Integer id;
    String text;
    @ManyToOne
    User receiver;
    @ManyToOne
    User sender;

    Integer chatId;

    @Enumerated(EnumType.STRING)
    MessageStatus status = MessageStatus.NEW;

    @CreationTimestamp
    LocalDateTime localDateTime = LocalDateTime.now();

    public Message(String text, User receiver, User sender) {
        this.text = text;
        this.receiver = receiver;
        this.sender = sender;
    }

    public Message(String text, User receiver, User sender, Integer chatId) {
        this.text = text;
        this.receiver = receiver;
        this.sender = sender;
        this.chatId = chatId;
    }

    public Message(String text, User receiver, User sender, Integer chatId, MessageStatus status, LocalDateTime localDateTime) {
        this.text = text;
        this.receiver = receiver;
        this.sender = sender;
        this.chatId = chatId;
        this.status = status;
        this.localDateTime = localDateTime;
    }

}

