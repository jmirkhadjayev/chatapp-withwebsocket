package uz.pdp.websocketdemo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

//JAMSHID MIRKHADJAYEV

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessageDto {
    private Integer id;
    private String text;
    private Integer receiverId;
    private Integer senderId;
    private String senderFullName;
    private String receiverFullName;
    private String status;
    private LocalDateTime localDateTime;



}
