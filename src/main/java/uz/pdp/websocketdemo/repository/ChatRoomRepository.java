package uz.pdp.websocketdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.websocketdemo.model.ChatRoom;

//JAMSHID MIRKHADJAYEV

public interface ChatRoomRepository extends JpaRepository<ChatRoom,  Integer> {

    boolean existsBySenderIdAndReceiverId(Integer sender_id, Integer receiver_id);
    ChatRoom findBySenderIdAndReceiverId(Integer sender_id, Integer receiver_id);

}
