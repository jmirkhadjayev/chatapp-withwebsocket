var stompClient = null;
var currentUser = null;

async function getCurrentUser() {
    await fetch('/api/users/me')
        .then(function (response) {
            response.json()
                .then(data => {
                    currentUser = data;
                })
        })
}

async function disconnect() {
    await fetch('/api/users/log')
        .then(function (response) {
            response.json()
                .then(data => {
                    currentUser = data;
                })
        })
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
    $("#users").empty()
}

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
        currentUser.online = true;
    } else {
        $("#conversation").hide();
        currentUser.online = true;
    }
    $("#greetings").html("");
}

function usersChangeHandle(selectedObj) {
    $("#greetings").empty();
    getMessagesByUserId(selectedObj.value);
}

function getMessagesByUserId(userId) {
    fetch("/api/messages/" + userId)
        .then(function (response) {
            if (response.ok) {
                response.json().then(res => {
                    res.map(value => {
                        appendNewMessage(value)
                    })
                })
            }
        })
}

function getWelcome() {
    $("#welcome").append(currentUser.fullName);
}

function getUsersFromServer() {
    fetch("/api/users")
        .then(function (response) {
            if (response.ok) {
                response.json().then(res => {
                    getMessagesByUserId(res[0].id);

                    res.map(value => {
                        showUsers(value)
                    })
                })
            }
        })
}

async function connect() {
    await getCurrentUser();

    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        getUsersFromServer();
        getWelcome();
        console.log('Connected: ' + frame);
        stompClient.subscribe(
            '/user/' + currentUser.id + '/queue/messages',
            function (messageDto) {
                const dataObj = JSON.parse(messageDto.body);
                if (dataObj.senderId == currentUser.id || dataObj.senderId == $("#users").val()) {

                    appendNewMessage(dataObj);
                } else
                    alert("New message | " + dataObj.receiverFullName)
            }
        );
    });
}

function sendMessage() {
    let dataStr = JSON.stringify(
        {
            'text': $("#name").val(),
            'receiverId': $("#users").val(),
            'receiverFullName': $("#users").find(":selected").text(),
            'senderId': currentUser.id,
            'senderFullName': currentUser.fullName
        }
    );
    stompClient.send(
        "/app/hello",
        {},
        dataStr
    );

    // appendNewMessage(JSON.parse(dataStr));
}

function appendNewMessage(message) {
    let tdAlign = message.senderId === currentUser.id ? 'right' : 'left';

    let status = message.status === 'READ' ? '✔✔' : message.status === 'NEW' ? '✔' : '⏱'
    let localtime = message.localDateTime
    $("#greetings").append("<tr>" +
        "<td align='" + tdAlign + "'>" +
        "<h6>" + message.senderFullName + "</h6>" +
        "<h3>" + message.text + "</h3>" +
        "<h6>" + status + "</h6>" +
        "<h6>" + localtime + "</h6>" +
        "</td>" +
        "</tr>");
}

function showUsers(user) {
    console.log(user);
    let online = user.online
    let text1 = '';
    if (online) {
        text1 = '🔹'
    }
    let result = user.id === currentUser.id ? "Saved Messages " : user.fullName;
    $("#users").append(
        "<option value=" + user.id + ">" + result + text1 + "</option>"
    );
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
    $("#send").click(function () {
        sendMessage();
    });
});